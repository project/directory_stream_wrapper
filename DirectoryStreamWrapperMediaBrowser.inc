<?php

/**
 * @file
 * Contains DirectoryStreamWrapperMediaBrowser.
 */

/**
 * Media browser plugin for remote files.
 */
class DirectoryStreamWrapperMediaBrowser extends MediaBrowserPlugin {
  /**
   * {@inheritdoc}.
   */
  public function access($account = NULL) {
   return user_access('administer files', $account);
  }

  /**
   * {@inheritdoc}.
   */
  public function view() {
    $build['form'] = drupal_get_form('directory_stream_wrapper_browse_form', $this->params);
    return $build;
  }
}
