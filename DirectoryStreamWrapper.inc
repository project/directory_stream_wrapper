<?php
/**
 * @file
 * Contains DirectoryStreamWrapper.
 */

/**
 * Directory stream wrapper.
 */
class DirectoryStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * {@inheritdoc}.
   */
  public function getDirectoryPath() {
    return DRUPAL_ROOT . '/' . variable_get('directory_stream_wrapper_directory');
  }

  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $GLOBALS['base_url'] . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
  }

  /**
   * {@inheritdoc}.
   */
  public function stream_write($data) {
    return FALSE;
  }

  /**
   * {@inheritdoc}.
   */
  public function unlink($uri) {
    // Although the file itself can't be deleted, return TRUE so that
    // file_delete() can remove the file record from the database.
    return TRUE;
  }

  /**
   * {@inheritdoc}.
   */
  public function rename($from_uri, $to_uri) {
    return FALSE;
  }

  /**
   * {@inheritdoc}.
   */
  public function mkdir($uri, $mode, $options) {
    return FALSE;
  }

  /**
   * {@inheritdoc}.
   */
  public function rmdir($uri, $options) {
    return FALSE;
  }

  /**
   * {@inheritdoc}.
   */
  public function chmod($mode) {
    return FALSE;
  }

  /**
   * {@inheritdoc}.
   */
  public function dirname($uri = NULL) {
    return FALSE;
  }
}

